import InewsDBQueue from "./InewsDBQueue";
import InewsQueueSubscription from "./InewsQueueSubscription";

// TODO allow inewsQueueSubscriptionOrOptions

class InewsDBQueueUpdater extends InewsDBQueue {
	constructor(filesDB, updatersDB, inewsQueueSubscription, options = {}) {
		super(filesDB, updatersDB, inewsQueueSubscription.inewsQueue, options);
		const self = this;
		self._canceled = false;

		// TODO handle fatal errors (like queue not found)

		self.inewsQueueSubscription = inewsQueueSubscription;

		self.inewsQueueSubscription.on('mutation', onMutation);
		self.inewsQueueSubscription.on('cycle-start', onCycleStart);
		//self.inewsQueueSubscription.on('list-end', onListEnd);
		self.inewsQueueSubscription.on('cycle-end', onCycleEnd);
		self.inewsQueueSubscription.on('cancel', onCancel);

		self.once('cancel', () => {
			self.inewsQueueSubscription.off('mutation', onMutation);
			self.inewsQueueSubscription.off('cycle-start', onCycleStart);
			//self.inewsQueueSubscription.off('list-end', onListEnd);
			self.inewsQueueSubscription.off('cycle-end', onCycleEnd);
			self.inewsQueueSubscription.off('cancel', onCancel);
		});

		async function onMutation(mutationType, fileName, mutationValue) {
			try {
				let filesDBDocID = self.filesDBDocID(fileName);
				let filesDBDocUpdates = {};
				let filesDBDoc = {};

				const inewsMutations = [];
				if(mutationType === InewsQueueSubscription.FILEMUTATIONTYPES.INSERT || mutationType === InewsQueueSubscription.FILEMUTATIONTYPES.UPDATE) {
					inewsMutations.push(mutationType);
					Object.assign(filesDBDocUpdates, mutationValue);
				}
				else if(mutationType === InewsQueueSubscription.FILEMUTATIONTYPES.DELETE)
					inewsMutations.push(mutationType);
				else if(mutationType === InewsQueueSubscription.FILEMUTATIONTYPES.ORDER) {
					inewsMutations.push(mutationType);
					if(mutationValue.hasOwnProperty('index')) {
						inewsMutations.push(InewsQueueSubscription.FILEMUTATIONTYPES.ORDERINDEX);
						filesDBDocUpdates.orderIndex = mutationValue.index;
					}

					if(mutationValue.hasOwnProperty('prev')) {
						inewsMutations.push(InewsQueueSubscription.FILEMUTATIONTYPES.ORDERPREV);
						filesDBDocUpdates.orderPrev = mutationValue.prev;
					}

					if(mutationValue.hasOwnProperty('next')) {
						inewsMutations.push(InewsQueueSubscription.FILEMUTATIONTYPES.ORDERNEXT);
						filesDBDocUpdates.orderNext = mutationValue.next;
					}
				}

				if(inewsMutations.length) {
					filesDBDocUpdates.inewsMutations = inewsMutations;

					try {
						filesDBDoc = await self.filesDB.get(filesDBDocID);
					}
					catch(error) {
						if(error.status !== 404 || (mutationType !== InewsQueueSubscription.FILEMUTATIONTYPES.INSERT && mutationType !== InewsQueueSubscription.FILEMUTATIONTYPES.UPDATE && mutationType !== InewsQueueSubscription.ITEMMUTATIONTYPES.DELETE))
							throw error;
					}

					Object.assign(filesDBDoc, filesDBDocUpdates);

					const filesDBDocWithFileName = self.filesDBDocAttachSelector(filesDBDoc, fileName);
					self.debug(`Mutating file ${filesDBDocWithFileName._id} with ${filesDBDocUpdates.inewsMutations.join(', ')}`);
					await self.filesDB.put(filesDBDocWithFileName);

					// Delete the item from the database
					if(mutationType === InewsQueueSubscription.FILEMUTATIONTYPES.DELETE) {
						filesDBDoc = await self.filesDB.get(filesDBDocID);
						self.debug(`Deleting file ${filesDBDoc._id}`);
						await self.filesDB.remove(filesDBDoc._id, filesDBDoc._rev);
					}
				}
			}
			catch(error) {
				self.emit('error', error);
			}
		}

		async function onCycleStart() {
			await updateQueueDBDoc({
				status: InewsDBQueueUpdater.STATUS.UPDATING,
				cycleStarted: (new Date()).toISOString()
			});
		}

		/* async function onListEnd() {
			await updateQueueDBDoc({
				status: InewsDBQueueUpdater.STATUS.UPDATING,
				listEnded: (new Date()).toISOString()
			});
		}

		 */

		async function onCycleEnd() {
			await updateQueueDBDoc({
				status: InewsDBQueueUpdater.STATUS.IDLE,
				cycleEnded: (new Date()).toISOString()
			});
		}

		async function onCancel() {
			await updateQueueDBDoc({
				status: InewsDBQueueUpdater.STATUS.CANCELED,
				canceled: (new Date()).toISOString()
			});
		}

		async function updateQueueDBDoc(updatersDBDocUpdate) {
			try {
				const updatersDBDocID = self.updatersDBDocID();
				let updatersDBDoc;
				try {
					updatersDBDoc = await self.updatersDB.get(updatersDBDocID);
				}
				catch(error) {
					if(error.status === 404)
						updatersDBDoc = {};
					else
						throw error;
				}
				Object.assign(updatersDBDoc, updatersDBDocUpdate);
				await self.updatersDB.put(self.updatersDBDocAttachSelector(updatersDBDoc));
			}
			catch(error) {
				self.emit('error', error);
			}
		}
	}

	cancel() {
		if(!this._canceled) {
			this._canceled = true;
			this.emit('cancel');
		}
	}

	get canceled() {
		return this._canceled;
	}

	debug() {
		if(this.options.hasOwnProperty('debug')) {
			if(typeof this.options.debug === 'function')
				this.options.debug.apply(this, Array.prototype.slice.call(arguments));
			else if(this.options.debug)
				console.log.apply(console, Array.prototype.slice.call(arguments));
		}
	}

	static get STATUS() {
		return Object.freeze({
			UPDATING: 'UPDATING',
			IDLE: 'IDLE',
			CANCELED: 'CANCELED',
		});
	}
}

export default InewsDBQueueUpdater;
