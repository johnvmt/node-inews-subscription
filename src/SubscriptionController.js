import EventEmitter from "wolfy87-eventemitter";

class SubscriptionController extends EventEmitter {
	constructor() {
		super();
		this._canceled = false;
	}

	get canceled() {
		return this._canceled;
	}

	cancel() {
		if(this.canceled)
			throw new Error('already_canceled');
		else {
			this._canceled = true;
			this.emit('cancel');
			this.removeAllListeners();
		}
	}

	error(error) {
		this.emit('error', error);
	}
}

export default SubscriptionController;
