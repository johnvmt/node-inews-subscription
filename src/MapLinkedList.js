class MapLinkedList extends Map {

	constructor() {
		super();

		this._orderedList = new Map();
		this.clear();
	}

	next(key) {
		return this.get(this.nextKey(key));
	}

	nextKey(key) {
		if(!this._orderedList.has(key))
			return undefined;

		return this._orderedList.get(key).nextKey;
	}

	prev(key) {
		return this.get(this.prevKey(key));
	}

	prevKey(key) {
		if(!this._orderedList.has(key))
			return undefined;

		return this._orderedList.get(key).prevKey;
	}

	clear() {
		this._orderedList.clear();
		this._orderedListHeadKey = null;
		this._orderedListTailKey = null;
		return super.clear();
	}

	set(key, value) {
		if(!this._orderedList.has(key)) {

			if(this._orderedListTailKey !== null)
				this._orderedList.get(this._orderedListTailKey).nextKey = key;

			let orderNode = {
				nextKey: null,
				prevKey: this._orderedListTailKey
			};

			this._orderedList.set(key, orderNode);

			this._orderedListTailKey = key;

			if(this._orderedListHeadKey === null)
				this._orderedListHeadKey = key;
		}

		return super.set(key, value);
	}

	delete(key) {
		if(this._orderedList.has(key)) {
			let orderNode = this._orderedList.get(key);

			let prevOrderNodeKey = orderNode.prevKey;
			let nextOrderNodeKey = orderNode.nextKey;

			if(prevOrderNodeKey !== null)
				this._orderedList.get(prevOrderNodeKey).nextKey = nextOrderNodeKey;

			if(nextOrderNodeKey !== null)
				this._orderedList.get(nextOrderNodeKey).prevKey = prevOrderNodeKey;

			if(key === this._orderedListHeadKey)
				this._orderedListHeadKey = orderNode.nextKey;

			if(key === this._orderedListTailKey)
				this._orderedListTailKey = orderNode.prevKey;
		}

		return super.delete(key);
	}
}

export default MapLinkedList;
