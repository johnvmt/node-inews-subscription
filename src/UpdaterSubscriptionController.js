import SubscriptionController from "./SubscriptionController";

class UpdaterSubscriptionController extends SubscriptionController {
	constructor() {
		super();
	}

	status(status, doc) {
		if(!this.canceled)
			this.emit('status', status, doc);
	}
}

export default UpdaterSubscriptionController;
