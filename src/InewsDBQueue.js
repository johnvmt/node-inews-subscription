import EventEmitter from "wolfy87-eventemitter";

class InewsDBQueue extends EventEmitter {
	constructor(filesDB, updatersDB, inewsQueue, options) {
		super();
		this.filesDB = filesDB;
		this.updatersDB = updatersDB;
		this.inewsQueue = inewsQueue;
		this.options = options;
	}

	async delete(deleteStoryDocs = true) {
		const updatersDBDocID = this.updatersDBDocID();

		try {
			this.updatersDB.delete(updatersDBDocID);
		}
		catch(error) {
			this.emit('error', error);
		}

		if(deleteStoryDocs) {
			const filesDBSelector = this.filesDBSelector();

			let storyDocsToDelete = (await this.filesDB.find({
				selector: filesDBSelector,
				fields: ['_id', '_rev']
			})).map((storyDoc) => {
				storyDoc._deleted = true;
				return storyDoc;
			});

			await this.filesDB.bulkDocs(storyDocsToDelete);
		}
	}

	// Add any prefix
	updatersDBDocID() {
		return `${this.options.hasOwnProperty('updatersDBDocPrefix') ? this.options.updatersDBDocPrefix : ""}${this.inewsQueue}`
	}

	// Add any prefix
	filesDBDocID(fileName) {
		return `${this.options.hasOwnProperty('filesDBDocPrefix') ? this.options.filesDBDocPrefix : ""}${this.inewsQueue}.${fileName}`
	}

	filesDBSelector(fileName = undefined) {
		return Object.assign({}, this.options.filesDBSelector, {inewsQueue: this.inewsQueue}, fileName === undefined ? {} : {fileName: fileName, _id: this.filesDBDocID(fileName)});
	}

	updatersDBSelector() {
		return Object.assign({}, this.options.updatersDBSelector, {inewsQueue: this.inewsQueue, _id: this.updatersDBDocID()});
	}

	filesDBDocAttachSelector(filesDBDoc, fileName = undefined) {
		return Object.assign({}, filesDBDoc, this.filesDBSelector(fileName));
	}

	updatersDBDocAttachSelector(updatersDBDoc) {
		return Object.assign({}, updatersDBDoc, this.updatersDBSelector());
	}

	debug() {
		if(this.options.hasOwnProperty('debug')) {
			if(typeof this.options.debug === 'function')
				this.options.debug.apply(this, Array.prototype.slice.call(arguments));
			else if(this.options.debug)
				console.log.apply(console, Array.prototype.slice.call(arguments));
		}
	}
}

export default InewsDBQueue;
