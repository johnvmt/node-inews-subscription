import EventEmitter from 'wolfy87-eventemitter';
import MapLinkedList from './MapLinkedList';

class InewsQueueSubscription extends EventEmitter {
    constructor(inewsClient, inewsQueue, subscriptionOptions) {
        super();

        const self = this;

	    self._inewsClient = inewsClient;
	    self._inewsClient.on('error', (error) => {
		    self.emit('error', error);
	    });

	    self._inewsQueue = inewsQueue;

	    self.options = Object.assign({
		    refreshInterval: 10000, // 10 seconds
		    insertOnList: true
	    }, subscriptionOptions);

	    self._canceled = false;
		self._firstCycleComplete = false;
	    self._queueFileOrder = new MapLinkedList();
	    self._storyGetPromises = new Set();

	    self.once('cycle-end', () => {
	    	self._firstCycleComplete = true;
	    });

		self.start();
    }

    get inewsQueue() {
    	return this._inewsQueue;
    }

	get status() {
    	return (this._canceled) ? 'canceled' : 'active';
	}

	async start() {
		while(this.status === 'active') {
			const cycleStartDate = new Date();

			try {
				this.emit('cycle-start');
				await this.cycle();
			}
			catch(error) {
				console.log("ERROR", error);
				this.emit('error', error);

				if(error === 'cwd_failed') // fatal errors
					this.cancel();
			}
			finally {
				this.emit('cycle-end');
				let cycleEndDate = new Date();
				if(this.status === 'active' && typeof this.options.refreshInterval === 'number' && (cycleEndDate - cycleStartDate) < this.options.refreshInterval)
					await delay(this.options.refreshInterval - (cycleEndDate - cycleStartDate)); // Delay next start
			}
		}

		function delay(ms) {
			return new Promise((resolve) => {
				setTimeout(() => {
					resolve();
				}, ms);
			});
		}
	}

	cycle() {
		const self = this;

		return new Promise(async (resolve, reject) => {
			try {
				const listItems = await self._inewsClient.list(self._inewsQueue);
				self.emit('list', listItems);

				// Convert directory listing to map of identifiers
				const previousQueueFileOrder = self._queueFileOrder;
				const currentQueueFileOrder =  new MapLinkedList();
				self._queueFileOrder = currentQueueFileOrder;

				// Build current queue file order
				for(let listItemIndex in listItems) {
					if(listItems.hasOwnProperty(listItemIndex)) {
						const listItem = listItems[listItemIndex];

						switch(listItem.fileType) {
							case 'DIRECTORY':
								currentQueueFileOrder.set(listItem.fileName, Object.assign(
									{},
									listItem,
									{
										orderIndex: Number(listItemIndex)
									}
								));
								break;
							case 'STORY':
								currentQueueFileOrder.set(listItem.identifier, Object.assign(
									{},
									listItem,
									{
										orderIndex: Number(listItemIndex)
									}
								));
								break;
							default:
								try {
									self.emit('error', new Error('unknown_file_type ' + JSON.stringify(listItem)))
								}
								catch(error) {}
								break;
						}
					}
				}

				// Check for deletions
				for(let listItemKeyVal of previousQueueFileOrder) {
					const listItemKey = listItemKeyVal[0];
					if(!currentQueueFileOrder.has(listItemKey)) // Story or directory removed
						self._emitItemMutation(InewsQueueSubscription.FILEMUTATIONTYPES.DELETE, listItemKey);
				}

				const storiesToGet = []; // story listItems
				// Check for insertions, story updates or order changes
				for(let listItemKeyVal of currentQueueFileOrder) {
					const listItemKey = listItemKeyVal[0];
					const listItem = listItemKeyVal[1];

					const currentVersionItem = Object.assign({},
						listItem,
						{
							orderNext: currentQueueFileOrder.nextKey(listItemKey),
							orderPrev: currentQueueFileOrder.prevKey(listItemKey)
						}
					);

					const previousVersionItem = (previousQueueFileOrder.has(listItemKey)) ? Object.assign(
						{},
						previousQueueFileOrder.get(listItemKey),
						{
							orderPrev: previousQueueFileOrder.prevKey(listItemKey),
							orderNext: previousQueueFileOrder.nextKey(listItemKey)
						}) : null;

					if(previousVersionItem === null && (self.options.insertOnList || currentVersionItem.fileType === 'DIRECTORY')) // inserted
						self._emitItemMutation(InewsQueueSubscription.FILEMUTATIONTYPES.INSERT, listItemKey, currentVersionItem);

					// NOTE: When flags or storyName/pageNumber change, the identifier changes too, so an update will be triggered

					if(previousVersionItem !== null) {
						let orderMutation = {};
						if(previousVersionItem.orderIndex !== currentVersionItem.orderIndex) {
							orderMutation.index = currentVersionItem.orderIndex;
							self._emitItemMutation(InewsQueueSubscription.FILEMUTATIONTYPES.ORDERINDEX, listItemKey, currentVersionItem.orderIndex);
						}

						if(previousVersionItem.orderPrev !== currentVersionItem.orderPrev) {
							orderMutation.prev = currentVersionItem.orderPrev;
							self._emitItemMutation(InewsQueueSubscription.FILEMUTATIONTYPES.ORDERPREV, listItemKey, currentVersionItem.orderPrev);
						}

						if(previousVersionItem.orderNext !== currentVersionItem.orderNext) {
							orderMutation.next = currentVersionItem.orderNext;
							self._emitItemMutation(InewsQueueSubscription.FILEMUTATIONTYPES.ORDERNEXT, listItemKey, currentVersionItem.orderNext);
						}

						if(Object.keys(orderMutation).length)
							self._emitItemMutation(InewsQueueSubscription.FILEMUTATIONTYPES.ORDER, listItemKey, orderMutation);
					}

					// update story
					if(currentVersionItem.fileType === 'STORY' && (previousVersionItem === null || (previousVersionItem.locator !== currentVersionItem.locator)))
						storiesToGet.push(currentVersionItem);
				}

				if(storiesToGet.length === 0)
					resolve();
				else {
					const storiesToGetFiltered = (typeof self.options.filterGet === 'function') ? await self.options.filterGet(storiesToGet) : storiesToGet;

					if(!Array.isArray(storiesToGetFiltered) || storiesToGetFiltered.length === 0)
						resolve();
					else {
						for(let listItemToGet of storiesToGetFiltered) {
							const storyGetPromise = self._inewsClient.story(self._inewsQueue, listItemToGet.fileName)
								.then(story => {
									const mutationType = (!self.options.insertOnList && !previousQueueFileOrder.has(listItemToGet.identifier)) ? InewsQueueSubscription.FILEMUTATIONTYPES.INSERT : InewsQueueSubscription.FILEMUTATIONTYPES.UPDATE;
									self._emitItemMutation(mutationType, listItemToGet.identifier, Object.assign({}, listItemToGet, {story: story}));
								})
								.catch(error => {
									self.emit('error', error);
								})
								.finally(() => {
									self._storyGetPromises.delete(storyGetPromise);
									if(self._storyGetPromises.size === 0)
										resolve();
								});

							self._storyGetPromises.add(storyGetPromise);
						}
					}
				}
			}
			catch(error) {
				console.log(error);
			}
		});
	}

    cancel() {
		if(this.status !== 'canceled') {
			this._canceled = true;

			for(let queueStoryPromise of this._queueStoryPromises) {
				queueStoryPromise.cancel();
			}

			this._queueStoryPromises.clear();
			this.emit('cancel');
		}
		else
			throw new Error('subscription_already_canceled');
    }

    _emitItemMutation(mutationType, fileName, mutationValue = null) {
    	this.emit('mutation', mutationType, fileName, mutationValue);
	}

	static get FILEMUTATIONTYPES() {
		return Object.freeze({
			INSERT: 'INSERT',
			UPDATE: 'UPDATE',
			DELETE: 'DELETE',
			ORDER: 'ORDER',
			ORDERINDEX: 'ORDERINDEX',
			ORDERPREV: 'ORDERPREV',
			ORDERNEXT: 'ORDERNEXT'
		});
	}
}

export default InewsQueueSubscription;
