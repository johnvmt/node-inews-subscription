import EventEmitter from 'wolfy87-eventemitter';
import InewsClient from 'inews';
import InewsQueueSubscription from './InewsQueueSubscription'

class _InewsConnectionSubscriptionManager extends EventEmitter {

	constructor(inewsConnectionOrOptions, subscriptionOptions = {}) {
		super();

		console.log("HIT");

		const self = this;

		self.subscriptionOptions = Object.assign({
			refreshInterval: 5000,
			insertExisting: true // Insert all stories found on first pass through queue
		}, subscriptionOptions);

		self._queueSubscriptionManagers = new Map();

		self.inewsConnection = (inewsConnectionOrOptions instanceof InewsClient) ? inewsConnectionOrOptions : new InewsClient(inewsConnectionOrOptions);

		self.inewsConnection.on('error', (error) => {
			self.emit('error', error);
		});

		self.inewsConnection.on('connected', () => {
			self.emit('connected');
		});

		self.inewsConnection.on('disconnected', () => {
			self.emit('disconnected');
		});
	}

	hasSubscription(inewsQueue) {
		let inewsQueueFormatted = _InewsConnectionSubscriptionManager.formatInewsQueue(inewsQueue);
		return this._queueSubscriptionManagers.has(inewsQueueFormatted)
	}

	subscription(inewsQueue) {
		let inewsQueueFormatted = _InewsConnectionSubscriptionManager.formatInewsQueue(inewsQueue);

		if(this.hasSubscription(inewsQueueFormatted))
			return this._queueSubscriptionManagers.get(inewsQueueFormatted);
		else
			throw new Error('not_subscribed');
	}

    /**
     * Create a real and virtual subscription, if they do not exist
     * @param inewsQueue
     * @param subscriptionOptions
     * @returns {*|Promise<PushSubscription>}
     */
	subscribe(inewsQueue, subscriptionOptions) {
	    let inewsQueueFormatted = _InewsConnectionSubscriptionManager.formatInewsQueue(inewsQueue);

	    if(!this.hasSubscription(inewsQueueFormatted)) {
			let queueSubscriptionOptions = Object.assign(this.subscriptionOptions, subscriptionOptions);
			const queueSubscriptionManager = new InewsQueueSubscription(this.inewsConnection, inewsQueueFormatted, queueSubscriptionOptions);
			this._queueSubscriptionManagers.set(inewsQueueFormatted, queueSubscriptionManager);
			return queueSubscriptionManager;
		}
	    else
			throw new Error('already_subscribed');
	}

	cancel() {
		const self = this;
		self._queueSubscriptionManagers.forEach((queueSubscriptionManager, inewsQueue) => {
			queueSubscriptionManager.cancel();
			self._queueSubscriptionManagers.delete(inewsQueue);
		});
    }

	static formatInewsQueue(inewsQueue) {
		return inewsQueue.toUpperCase();
	}
}

export default _InewsConnectionSubscriptionManager;
