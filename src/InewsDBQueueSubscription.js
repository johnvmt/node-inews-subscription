import PouchDBSubscription from "pouchdb-subscription";
import InewsDBQueue from "./InewsDBQueue";
import UpdaterSubscriptionController from "./UpdaterSubscriptionController";
import FilesSubscriptionController from "./FilesSubscriptionController";

class InewsDBQueueSubscription extends InewsDBQueue {

	constructor(filesDB, updatersDB, inewsQueue, options) {
		super(filesDB, updatersDB, inewsQueue, options);

		this._subscriptionControllers = new Set();

		this._filesSubscriptionMaster = new PouchDBSubscription(filesDB, {
			find: true,
			selector: this.filesDBSelector()
		});

		this._updatersSubscriptionMaster = new PouchDBSubscription(updatersDB, {
			find: true,
			selector: this.updatersDBSelector()
		});

		console.log("DEBUG?-INEWSDBQUEUESUBSCRIPTION", options.debug);
	}

	async subscribeUpdater() {
		const dbUpdaterSubscriptionController = await this._updatersSubscriptionMaster.subscribe(0);
		const updaterSubscriptionController = new UpdaterSubscriptionController();

		updaterSubscriptionController.once('cancel', () => {
			dbUpdaterSubscriptionController.cancel();
		});

		dbUpdaterSubscriptionController.on('mutation', (dbMutation, doc) => {
			updaterSubscriptionController.status(doc.status, doc);
		});

		return updaterSubscriptionController;
	}

	async subscribeFiles(since = 0) {
		const dbFilesSubscriptionController = await this._filesSubscriptionMaster.subscribe(since);
		const filesSubscriptionController = new FilesSubscriptionController();

		filesSubscriptionController.once('cancel', () => {
			dbFilesSubscriptionController.cancel();
		});

		// TODO everything before find-complete gets an iNews mutation of UPDATE (or maybe DELETE?)
		dbFilesSubscriptionController.once('find-complete', () => {
			filesSubscriptionController.mutation(['SYNC'], null, null);
		});

		dbFilesSubscriptionController.on('mutation', (dbMutation, fileDoc, seq) => {
			if(fileDoc.hasOwnProperty('inewsMutations')) {

				filesSubscriptionController.mutation(fileDoc.inewsMutations, fileDoc, seq);
			}

			else {
				console.log(dbMutation, fileDoc, seq);
				console.log("MISSING iNews Mutations");
			}
		});

		return filesSubscriptionController;
	}
}

export default InewsDBQueueSubscription;
