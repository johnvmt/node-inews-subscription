import SubscriptionController from "./SubscriptionController";

class FilesSubscriptionController extends SubscriptionController {
	constructor() {
		super();
	}

	mutation(mutation, doc, seq = null) {
		if(!this.canceled)
			this.emit('mutation', mutation, doc, seq);
	}
}

export default FilesSubscriptionController;
