## node-inews-subscription

* Uses an [inews-ftp](https://www.npmjs.com/package/inews) connection

## Example

	const manager = new InewsConnectionSubscriptionManager({
		"hosts": [
			"inews-a.mydomain.com",
			"inews-b.mydomain.com",
			"inews-c.mydomain.com"
		],
		"user": "username",
		"password": "password"
	});
	
	let subscription = manager.subscribe('PATH.TO.MY.RUNDOWN');
	
	subscription.on('cancel', () => {
		console.log("Canceled");
	});
	
	subscription.on('error', error => {
		console.error("Error", error);
	});
	
	subscription.on('cycle-start', () => {
		console.log("Cycle Started");
	});
	
	subscription.on('cycle-end', () => {
		console.log("Cycle Ended");
	});
	
	subscription.on('mutation', async (data) => {
		let docId = data.object.filename;
	
		// TODO filter object
		const disallowedKeys = ['order', 'prev'];
	
		const filteredObject = Object.keys(data.object)
			.filter(key => !disallowedKeys.includes(key))
			.reduce((object, key) => {
				object[key] = data.object[key];
				return object;
			}, {});
	
		let doc;
	
		//console.log(data.mutation);
	
		switch(data.mutation) {
			case 'next':
				doc = await db.get(docId);
				await db.put(Object.assign({
					next: filteredObject.next
				}, doc));
				break;
			case 'insert':
				await db.put(Object.assign(filteredObject, {_id: docId}));
				break;
			case 'update':
				doc = await db.get(docId);
				await db.put(Object.assign(filteredObject, {
					_id: doc._id,
					_rev: doc._rev
				}));
				break;
			case 'delete':
				doc = await db.get(docId);
				await db.remove(doc._id, doc._rev);
				break;
		}
	});
